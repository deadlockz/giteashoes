﻿using System;
using Gtk;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace giteaShues
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Application.Init();
            MainWindow win = new MainWindow();
            string[] arguments = Environment.GetCommandLineArgs();
            if (arguments.Length < 6)
            {
                Console.Out.WriteLine(
                    String.Format("{0} Win|Linux|Osx \"ddd, dd.MM. HH:mm\" https://domain:port/path/ token00api23key42etc999 path/to/repolist.txt", arguments[0])
                );
                return;
            }
            else
            {
                win.OSPLAT = arguments[1];
                win.TIMEFORMAT = arguments[2];
                win.APIHOST = arguments[3];
                win.APPTOKEN = arguments[4];
                win.REPOLIST = arguments[5];
            }

            win.Show();
            Application.Run();
        }
    }
}
