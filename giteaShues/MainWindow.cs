﻿using System;
using System.Diagnostics;
using System.Net;
using Gtk;
using Newtonsoft.Json.Linq;

public partial class MainWindow : Gtk.Window
{
    private const string APIWORD = "api/v1/repos/{0}/issues";
    private ListStore model;

    public string APIHOST = "";
    public string APPTOKEN = "";
    public string OSPLAT = "";
    public string TIMEFORMAT = "";
    public string REPOLIST = "";

    public class Issue
    {
        // StopWatch represent a running stopwatch
        public DateTimeOffset created_at { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string url { get; set; }
        public string repo { get; set; }
    }

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
        ServicePointManager.ServerCertificateValidationCallback += (send, certificate, chain, sslPolicyErrors) => { return true; };

        TreeViewColumn c1 = new TreeViewColumn();
        c1.Title = "id";
        CellRendererText cr1 = new CellRendererText();
        c1.PackStart(cr1, true);
        c1.AddAttribute(cr1, "text", 0);

        TreeViewColumn c2 = new TreeViewColumn();
        c2.Title = "Repo";
        CellRendererText cr2 = new CellRendererText();
        c2.PackStart(cr2, true);
        c2.AddAttribute(cr2, "text", 1);

        TreeViewColumn c3 = new TreeViewColumn();
        c3.Title = "title";
        CellRendererText cr3 = new CellRendererText();
        c3.PackStart(cr3, true);
        c3.AddAttribute(cr3, "text", 2);

        TreeViewColumn c4 = new TreeViewColumn();
        c4.Title = "url";
        CellRendererText cr4 = new CellRendererText();
        c4.PackStart(cr4, true);
        c4.AddAttribute(cr4, "text", 3);

        treeview1.AppendColumn(c1);
        treeview1.AppendColumn(c2);
        treeview1.AppendColumn(c3);
        treeview1.AppendColumn(c4);

        model = new ListStore(typeof(int), typeof(string), typeof(string), typeof(string));
        treeview1.Model = model;
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    private void urlOpen(string url)
    {
        try
        {
            Process.Start(url);
        }
        catch (Exception)
        {
            if (OSPLAT == "Win")
            {
                url = url.Replace("&", "^&");
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }
            else if (OSPLAT == "Linux")
            {
                Process.Start("xdg-open", url);
            }
            else if (OSPLAT == "Osx")
            {
                Process.Start("open", url);
            }
            else
            {
                Console.Out.WriteLine("Fail to start with browser: " + url);
            }
        }
    }

    protected void OnButton1Clicked(object sender, EventArgs e)
    {
        JArray datas = new JArray();
        model.Clear();

        string[] repos = System.IO.File.ReadAllLines(REPOLIST);

        foreach (string line in repos)
        { 
            if (line.StartsWith("#") == false && line.Length > 3)
            {
                string repo = String.Format(APIWORD, line);
                using (var client = new WebClient())
                {
                    // fetch notifications
                    string req = String.Format("{0}{1}?token={2}", APIHOST, repo, APPTOKEN);
                    Console.Out.WriteLine(req);
                    string response = client.DownloadString(req);
                    datas = JArray.Parse(response);
                }

                foreach (JObject data in datas)
                {
                    Issue isu = new Issue();
                    isu.id = data["id"].ToObject<int>();
                    isu.body = data["body"].ToString();
                    isu.title = data["title"].ToString();
                    isu.created_at = data["created_at"].ToObject<DateTimeOffset>();
                    isu.url = data["html_url"].ToString();
                    isu.repo = line;

                    model.AppendValues(isu.id, isu.repo, isu.title, isu.url);
                }
            }
        }
    }

    protected void OnButton2Clicked(object sender, EventArgs e)
    { 
        foreach (var item in treeview1.Selection.GetSelectedRows())
        {
            TreeIter iter;
            model.GetIter(out iter, item);
            urlOpen(model.GetValue(iter, 3).ToString());
        }
    }
}
